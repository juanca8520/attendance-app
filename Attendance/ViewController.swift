//
//  ViewController.swift
//  Attendance
//
//  Created by Juan Camilo Pulido  on 8/10/19.
//  Copyright © 2019 Juan Camilo Pulido . All rights reserved.
//

import UIKit
import CoreBluetooth

struct Att: Decodable {
    let name: String
    let createTime: String
    let updateTime: String
}

class ViewController: UIViewController, UITextFieldDelegate {
    
    var centralManager: CBCentralManager!
    
    var dbMessage = "Respuesta de la base de datos: " {
        didSet{
            dbMessageLabel.text = "Respuesta de la base de datos: \(dbMessage)"
        }
    }
    
    @IBOutlet weak var dbMessageLabel: UILabel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        let Tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        
        view.addGestureRecognizer(Tap)
        
    }
    
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
  
    @IBOutlet weak var codeTextField: UITextField!
    
    
    @IBAction func checkAttendance(_ sender: UIButton) {
        dbMessageLabel.numberOfLines = 0
        dbMessageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        
        let studentCode = String(codeTextField.text!)
        codeTextField.resignFirstResponder()
        let date = Date()
        
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = (calendar.component(.month, from: date)) - 1
        let year = calendar.component(.year, from: date)
        
        let dateF = "\(day)-\(month)-\(year)"
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/\(dateF)/students/\(studentCode)")!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            
            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    print(json)
                    DispatchQueue.main.async {
                        self.dbMessage = "\(json)"
                    }
                }
                
                
                //Este sirve muy bien, pero no pude manejar el objeto dentro del JSON, TODO: Preguntar bien
//                let att = try JSONDecoder().decode(Att.self, from: data)
//                print(att)
                
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    @IBAction func activateDiscoverable(_ sender: UIButton) {
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }
}

extension UIViewController: CBCentralManagerDelegate {
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state{
        case .unknown:
            print("Central.state is unknown")
        case .resetting:
            print("Central.state is resetting")
        case .unsupported:
            print("Central.state is unsupported")
        case .unauthorized:
            print("Central.state is unauthorized")
        case .poweredOff:
            print("Central.state is powered off")
        case .poweredOn:
            print("Central.state is poweredOn")
            central.scanForPeripherals(withServices: nil)
        }
    }
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //print(peripheral)
    }
    
}

